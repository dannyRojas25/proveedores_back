-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.3.16-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para proveedoresDB
CREATE DATABASE IF NOT EXISTS `proveedoresdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `proveedoresDB`;

-- Volcando estructura para tabla proveedoresDB.t_proveedores
CREATE TABLE IF NOT EXISTS `t_proveedores` (
  `tp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tp_name` varchar(200) NOT NULL,
  `tp_date` datetime DEFAULT NULL,
  `tp_phone` varchar(7) NOT NULL,
  `tp_address` varchar(150) NOT NULL,
  PRIMARY KEY (`tp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla proveedoresDB.t_proveedores: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `t_proveedores` DISABLE KEYS */;
INSERT INTO `t_proveedores` (`tp_id`, `tp_name`, `tp_date`, `tp_phone`, `tp_address`) VALUES
	(1, 'Suramericana de seguros', '2019-12-14 09:41:38', '57624', 'CL 46 # 23 -19'),
	(2, 'Bocadillos el caribe', '2019-12-14 09:43:10', '31878', 'Cl 23 ');
/*!40000 ALTER TABLE `t_proveedores` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
